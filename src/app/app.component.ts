import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.scss'],
  template: `
    <div class="app">
      <header class="app-header">
        <div class="app-container">
          <img [src]="logo" alt="Logo" />
          <nav class="app-nav">
            <a [routerLink]="['home']">HOME</a>
            <a [routerLink]="['characters']">CHARACTERS</a>
            <a [routerLink]="['games']">GAMES</a>
            <a [routerLink]="['re3']">RE3</a>
          </nav>
        </div>
      </header>
      <router-outlet></router-outlet>
      <footer>
        <img [src]="logo" />
      </footer>
    </div>
  `,
})
export class AppComponent {
  logo: string = '/assets/images/capcom.png';
}
