import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  AfterViewInit,
  Renderer2,
} from '@angular/core';

@Component({
  selector: 'banner-component',
  styleUrls: ['./banner.component.scss'],
  template: `
    <section class="banner" #banner>
      <div class="banner-content">
        <img [src]="logo" />
      </div>
    </section>
  `,
})
export class BannerComponent implements AfterViewInit {
  @Input() logo: string = '/assets/images/logo_re2.png';
  @Input() bannerImage: string = '/assets/images/re2_banner.jpg';
  @ViewChild('banner') bannerElement: ElementRef<HTMLElement>;

  constructor(private renderer: Renderer2) {}

  ngAfterViewInit() {
    this.renderer.setStyle(
      this.bannerElement.nativeElement,
      'background-image',
      `url(${this.bannerImage})`
    );
  }
}
