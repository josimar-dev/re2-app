import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardComponent } from './card';
import { BannerComponent } from './banner';

@NgModule({
  declarations: [CardComponent, BannerComponent],
  imports: [CommonModule],
  exports: [CardComponent, BannerComponent],
})
export class UIModule {}
