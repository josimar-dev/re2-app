import { Component, Input } from '@angular/core';

@Component({
  selector: 'card-component',
  styleUrls: ['./card.component.scss'],
  template: `
    <div
      class="card"
      [ngClass]="{
        'card-small': small,
        'card-medium': medium,
        'card-large': large
      }"
    >
      <img [src]="image" />
    </div>
  `,
})
export class CardComponent {
  @Input() small: boolean = false;
  @Input() medium: boolean = false;
  @Input() large: boolean = false;
  @Input() image: string = '';
}
