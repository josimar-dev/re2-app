import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { CharactersService } from '../../shared/characters.service';
import { Character } from '../../shared';

@Component({
  styleUrls: ['./characters.component.scss'],
  template: ` <div>
    <h1>CHARACTERS PAGE</h1>
    <a [routerLink]="['character', 1]">LINK</a>
    <p>{{ characters | json }}</p>
  </div>`,
  providers: [CharactersService],
})
export class CharactersComponent implements OnInit, OnDestroy {
  subs = new SubSink();
  characters: Character[];

  constructor(private charactersService: CharactersService) {}

  ngOnInit() {
    this.subs.add(
      this.charactersService.getR2Characters().subscribe((characters) => {
        this.characters = characters;
      })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
