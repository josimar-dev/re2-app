import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharactersService } from '../../shared/characters.service';
import { SubSink } from 'subsink';
import { Character } from '../../shared';

@Component({
  template: `
    <character-details
      *ngIf="character"
      [name]="character.fullName"
      [imagePath]="character.picture"
      [about]="character.description"
    ></character-details>
  `,
  styleUrls: ['./character.component.scss'],
  providers: [CharactersService],
})
export class CharacterComponent implements OnInit, OnDestroy {
  subs = new SubSink();
  character: Character;
  constructor(
    private route: ActivatedRoute,
    private charactersService: CharactersService
  ) {}

  ngOnInit() {
    this.subs.add(
      this.route.params.subscribe((params) => {
        this.subs.add(
          this.charactersService
            .getCharacter(params.id)
            .subscribe((character) => {
              console.log(character[0]);
              this.character = character[0];
            })
        );
      })
    );
    console.log(this.character);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
