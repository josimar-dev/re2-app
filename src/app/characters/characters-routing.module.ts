import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { UIModule } from '@app/ui/ui.module';

import {
  CharacterComponent,
  CharactersComponent,
  CharacterDetailsComponent,
} from './shared';

const routes: Routes = [
  { path: '', component: CharactersComponent },
  { path: 'character', component: CharacterComponent },
  { path: 'character/:id', component: CharacterComponent },
];

@NgModule({
  declarations: [
    CharacterComponent,
    CharactersComponent,
    CharacterDetailsComponent,
  ],
  imports: [CommonModule, RouterModule.forChild(routes), UIModule],
})
export class CharactersRoutingModule {}
