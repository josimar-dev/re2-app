import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { Character } from './interface';

@Injectable({
  providedIn: 'root',
})
export class CharactersService {
  constructor(private http: HttpClient) {}

  getR2Characters(): Observable<Character[]> {
    return this.http.get<Character[]>(`${environment.reAPI}/re2_characters`);
  }

  getCharacter(characterId: string): Observable<Character> {
    return this.http.get<Character>(
      `${environment.reAPI}/re2_characters?id=${characterId}`
    );
  }

  getR3Characters(): Observable<Character[]> {
    return this.http.get<Character[]>(`${environment.reAPI}/re3_characters`);
  }
}
