export * from '../pages/character/character.component';
export * from '../pages/characters/characters.component';
export * from '../components/character-details/character-details.component';
export * from './interface';
