export interface Character {
  id: number;
  fullName: string;
  name: string;
  description: string;
  picture: string;
}
