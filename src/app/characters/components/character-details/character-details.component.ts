import { Component, Input } from '@angular/core';

@Component({
  selector: 'character-details',
  template: `
    <section class="character">
      <div class="container">
        <div class="character-content">
          <div class="character-content-image">
            <img [src]="imagePath" />
          </div>
          <div class="character-content-info">
            <h1>{{ name }}</h1>
            <p>{{ about }}</p>
          </div>
        </div>
      </div>
    </section>
  `,
  styleUrls: ['./character-details.component.scss'],
})
export class CharacterDetailsComponent {
  @Input() imagePath: string = '';
  @Input() about: string = '';
  @Input() name: string = '';
}
