import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { UIModule } from '@app/ui/ui.module';

import { Re3PageComponent } from './re3-page.component';

const routes: Routes = [{ path: '', component: Re3PageComponent }];

@NgModule({
  declarations: [Re3PageComponent],
  imports: [UIModule, CommonModule, RouterModule.forChild(routes)],
})
export class Re3PageRoutingModule {}
