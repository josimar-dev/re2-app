import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';

import { Character, Banner, Logo } from './interfaces';

@Injectable({
  providedIn: 'root',
})
export class Re3PageService {
  constructor(private http: HttpClient) {}

  getCharacters(): Observable<Character[]> {
    return this.http.get<Character[]>(`${environment.reAPI}/re3_characters`);
  }

  getBanner(): Observable<Banner> {
    return this.http.get<Banner>(`${environment.reAPI}/re3_banner`);
  }

  getLogo(): Observable<Logo> {
    return this.http.get<Logo>(`${environment.reAPI}/re3_logo`);
  }
}
