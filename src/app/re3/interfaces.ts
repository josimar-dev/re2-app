export interface Character {
  id: number;
  fullName: string;
  name: string;
  description: string;
  picture: string;
}

export interface Banner {
  path: string;
}

export interface Logo {
  path: string;
}
