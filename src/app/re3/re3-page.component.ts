import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { Re3PageService } from './re3-page.service';
import { Character, Banner, Logo } from './interfaces';

@Component({
  template: `
    <div *ngIf="banner && logo">
      <banner-component
        [bannerImage]="'/assets/images/re3_banner.jpg'"
        [logo]="logo.path"
      ></banner-component>
    </div>

    <!-- Gotta create it as a component -->
    <section class="characters">
      <div class="container">
        <h1>CHARACTERS</h1>
        <ul class="container-list">
          <li *ngFor="let character of characters">
            <card-component
              [small]="true"
              [image]="character.picture"
            ></card-component>
            <div class="description">
              <h2>{{ character.fullName }}</h2>
              <p>{{ character.description }}</p>
            </div>
          </li>
        </ul>
      </div>
    </section>
  `,
  styleUrls: ['./re3-page.component.scss'],
  providers: [Re3PageService],
})
export class Re3PageComponent implements OnInit, OnDestroy {
  subs = new SubSink();
  banner: Banner;
  logo: Logo;
  characters: Character[];
  constructor(private re3Service: Re3PageService) {}

  ngOnInit() {
    this.subs.add(
      this.re3Service.getCharacters().subscribe((characters) => {
        this.characters = characters;
      })
    );
    this.subs.add(
      this.re3Service.getBanner().subscribe((banner) => {
        this.banner = banner;
        console.log(banner.path);
      })
    );
    this.subs.add(
      this.re3Service.getLogo().subscribe((logo) => {
        this.logo = logo;
      })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
