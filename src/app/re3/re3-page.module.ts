import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Re3PageRoutingModule } from './re3-page-routing.module';

@NgModule({
  imports: [CommonModule, Re3PageRoutingModule],
})
export class Re3PageModule {}
