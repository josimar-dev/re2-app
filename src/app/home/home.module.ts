import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home.component';
import { UIModule } from '../ui/ui.module';

@NgModule({
  declarations: [HomeComponent],
  imports: [UIModule, CommonModule],
  exports: [HomeComponent],
})
export class HomeModule {}
