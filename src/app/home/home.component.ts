import { Component } from '@angular/core';

@Component({
  styleUrls: ['./home.component.scss'],
  template: `<banner-component></banner-component>`,
})
export class HomeComponent {}
