import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule,
  PreloadingStrategy,
  Route,
} from '@angular/router';
import { Observable, of } from 'rxjs';

export class PreloadSelectedModulesList implements PreloadingStrategy {
  preload(route: Route, load: Function): Observable<any> {
    return route.data && route.data['preload'] ? load() : of(null);
  }
}

import { HomeComponent } from './home/home.component';
import { HomeModule } from './home/home.module';

const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'characters',
    loadChildren: async () =>
      (await import('./characters/characters.module')).CharactersModule,
    data: { preload: true },
  },
  {
    path: 're3',
    loadChildren: async () =>
      (await import('./re3/re3-page.module')).Re3PageModule,
  },
  { path: '**', component: HomeComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadSelectedModulesList,
    }),
    HomeModule,
  ],
  providers: [PreloadSelectedModulesList],
  exports: [RouterModule],
})
export class AppRoutingModule {}
