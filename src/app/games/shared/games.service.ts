import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Game } from './interface';

@Injectable({
  providedIn: 'root',
})
export class GamesService {
  constructor(private http: HttpClient) {}

  getR2Games(): Observable<Game[]> {
    return this.http.get<Game[]>(`${environment.reAPI}/re2_games`);
  }
}
