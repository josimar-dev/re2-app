export interface Game {
  id: number;
  title: string;
  releaseDate: string;
  platform: string;
  description: string;
  cover: string;
}
